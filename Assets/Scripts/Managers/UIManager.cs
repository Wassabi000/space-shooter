﻿using System;
using UnityEngine;

namespace Managers
{
    public class UIManager : MonoBehaviour
    {
        public GameObject gameOverPanel;

        private void Awake()
        {
            EventManager.OnGameOverEvent += ShowGameOverMenu;
        }

        private void ShowGameOverMenu()
        {
            gameOverPanel.SetActive(true);
        }

        public void StartGameButton()
        {
            gameOverPanel.SetActive(false);
        }
    }
}
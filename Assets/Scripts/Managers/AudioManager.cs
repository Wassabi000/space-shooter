﻿using System;
using UnityEngine;

namespace Managers
{
    public enum SoundEffect
    {
        Unassigned,
        Explosion,
        RockHit,
        RockBreak,
        LaserHit,
        LaserFast,
        LaserHeavy,
        LaserDefault
    }


    public class AudioManager : MonoBehaviour
    {

        public AudioSource audioSource;
        
        public AudioClip[] audioClips;


        public void PlaySound(SoundEffect passedSound)
        {
            var clipID = (int) passedSound;
            
            audioSource.clip = audioClips[ clipID  ];
            audioSource.Play();
        }
    }
}
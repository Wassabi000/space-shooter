﻿using System;
using System.Collections;
using System.Collections.Generic;
using Combat;
using Enemy;
using Obstacles;
using Paths;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Managers
{
    public enum EnemyType
    {
        Bomber,
        Scorpion,
        Viper
    }

    public class EnemyManager : MonoBehaviour
    {
        
        
        [SerializeField] private Transform formationPosition;
        [SerializeField] private Transform[] spawnPositions;
        [SerializeField] private List<EnemyController> enemyTypes = new List<EnemyController>();
        
        [SerializeField] private Transform[] paths; 
        [SerializeField] private Wave[] waves;
        [SerializeField] private Formation[] formations;
        
        private List<EnemyController> _enemiesWave;
        private List<EnemyController> _allowedEnemies = new List<EnemyController>();

        private void Awake()
        {
            foreach (var enemyID in GameManager.Instance.GetAllowedEnemies())
            {
                _allowedEnemies.Add( enemyTypes[enemyID] );
            }
        }

        public void StartWave() 
        {
            var spawnPosition = spawnPositions[0];
            var waveIndex = Random.Range(0, waves.Length);
            var newWave = Instantiate( waves[waveIndex], spawnPosition);
            newWave.enemyToSpawn = GetRandomAvailableEnemy();
            newWave.GetComponent<Wave>().InitializeWave(spawnPositions[0]);
        }

        // Starts formation 
        public void StartFormation()
        {
            var formationIndex = Random.Range(0, formations.Length);
            var newFormation = Instantiate( formations[formationIndex], formationPosition);
            newFormation.enemyToSpawn = GetRandomAvailableEnemy();
            StartCoroutine( newFormation.GetComponent<Formation>().InitializeFormation(spawnPositions) );
        }

        private EnemyController GetRandomAvailableEnemy()
        {
            if (_allowedEnemies.Count > 1)
            {
                return _allowedEnemies[Random.Range(0, _allowedEnemies.Count)];
            } else {
                return _allowedEnemies[0];
            }
        }
    }
}

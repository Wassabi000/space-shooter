﻿using System;
using System.Collections;
using System.Collections.Generic;
using Obstacles;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Managers
{
    public class ObstacleManager : MonoBehaviour
    {
        [SerializeField] private List<GameObject> obstaclePrefabs = new List<GameObject>();
        [SerializeField] private Transform spawnPoint;
        [SerializeField] private Transform destinationPoint;
        private List<GameObject> _spawnedObstacles = new List<GameObject>();
        
        [Range(0.1f, 5f)]
        [SerializeField] float spawnRate;
        // [Range(5f, 10f)]
        // [SerializeField] float maxSpawnRate;
        
        [Range(0.1f, 0.15f)]
        [SerializeField] float speed;
        // [Range(0.25f, 0.5f)]
        // [SerializeField] float maxSpeed;
    
    
        public void StartMeteorShower(float duration)
        {
            StartCoroutine(GenerateObstaclesOvertime(duration) );
        }

        private IEnumerator GenerateObstaclesOvertime(float duration)
        {
            var startTime = Time.time;
            
            
            while (Time.time < (startTime + duration) )
            {
                var randomWaitForSeconds = new WaitForSeconds( Random.Range(spawnRate, spawnRate * 2) );
                yield return randomWaitForSeconds;

                // Debug.Log("Spawning obstacle");
                GenerateObstacle();
            }
        }

        private void GenerateObstacle()
        {
            var obstacleGO = obstaclePrefabs[ Random.Range(0, obstaclePrefabs.Count) ]; // Gets a random obstacle gameObject from the list
            
            // for tidiness
            var start = spawnPoint.position;
            var end = destinationPoint.position;
            
            var randomSpawnPosition =  new Vector3( Random.Range(-start.x, start.x ), start.y, 0); // Set random spawn position at the top of the screen
            var randomDestinationPosition =  new Vector3( Random.Range(-end.x, end.x ), end.y, 0); // Set random destination position at the bottom of the screen
            
            // TODO - Cosmetic: figure out how to avoid obstacles never appearing on the screen by always being outside of game screen
            // Possibly to create additional spawn points on sides of the screen and narrow down the destination coordinate spread

            var spawnedObstacle = Instantiate(obstacleGO, spawnPoint); // Instantiate the obstacle
            spawnedObstacle.transform.position = randomSpawnPosition;
            
            var obstacleController = spawnedObstacle.GetComponent<ObstacleController>();
            
            obstacleController.destinationPoint = randomDestinationPosition; // Set the destination
            obstacleController.speed = Random.Range(speed, speed * 2);
        }
    }
}

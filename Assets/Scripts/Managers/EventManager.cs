﻿using System;
using UnityEngine;

namespace Managers
{
    public class EventManager : MonoBehaviour
    {
        public static EventManager Instance;

        #region Events definition
        
        public static event Action OnGameStartedEvent;
        public static event Action OnGameOverEvent;
        public static event Action<int> OnScoreAddedEvent;

        #region Gameplay events

        public static event Action OnFormationClearedEvent;
        public static event Action<float> OnFuelDecreasedEvent;
        public static event Action<float> OnPlayerHealthChangedEvent;

        #endregion
        
        #endregion
        
        private void Awake()
        {
            if (Instance == null)
                Instance = this;
        }

        #region Event delegates

        public void OnGameStarted()
        {
            OnGameStartedEvent?.Invoke();
        }
        
        public void OnGameOver()
        {
            OnGameOverEvent?.Invoke(); // If not null, invoke
        }

        public static void OnScoreAdded(int scoreToAdd)
        {
            OnScoreAddedEvent?.Invoke(scoreToAdd); // If not null, invoke
        }

        #region Gameplay event delegates

        public static void OnFormationCleared()
        {
            OnFormationClearedEvent?.Invoke();
        }
        
        public static void OnFuelDecreased(float fuelStatFuel)
        {
            OnFuelDecreasedEvent?.Invoke(fuelStatFuel);
        }

        public void OnPlayerHealthChanged(float currentHp)
        {
            OnPlayerHealthChangedEvent?.Invoke(currentHp);
        }

        #endregion
        
        #endregion

        
    }
}
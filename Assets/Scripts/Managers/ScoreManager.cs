﻿using System;
using UnityEngine;

namespace Managers
{
    public class ScoreManager : MonoBehaviour
    {
        public static ScoreManager Instance;
        
        private double _currentScore;


        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            
            EventManager.OnScoreAddedEvent += AddToScore;
        }

        public void AddToScore(int scoreToAdd)
        {
            _currentScore += scoreToAdd;
        }
    }
}

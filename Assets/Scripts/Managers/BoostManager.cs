﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Combat;
using Unity.Mathematics;
using UnityEngine.UIElements;
using Random = UnityEngine.Random;

namespace Managers
{
    public enum BoostType
    {
        Shield,
        Fuel,
        Repair,
        Armor,
        Weapon
    }
    public class BoostManager : MonoBehaviour
    {
        public Boost[] boostPrefabs;

        private List<Boost> _allowedBoosts;
        private List<int> _totalChance = new List<int>();
        
        
        private void Start()
        {
            _allowedBoosts = boostPrefabs.ToList();
            CalculateTotalChance();
        }

        private void CalculateTotalChance()
        {
            _totalChance.Clear();
            
            for (var i = 0; i < _allowedBoosts.Count; i++)
            {
                var boost = _allowedBoosts[i];
                
                for (var j = 0; j < boost.spawnChance; j++)
                    _totalChance.Add(i);
            }
        }


        public void TrySpawningBoost(Transform boostPosition)
        {
            if (Random.Range(0, 100) >= 25) return;
            
            // Spawn boost
            // Check which boost to spawn
            var randomID = Random.Range(0, _totalChance.Count);
            var boostToSpawn = _allowedBoosts[ _totalChance[randomID] ];
                
            Instantiate( boostToSpawn, boostPosition.position, Quaternion.identity);
        }

        public void StopArmorBoost()
        {
            _allowedBoosts.Remove( _allowedBoosts.Find(o => o.boostType == BoostType.Armor) );
            CalculateTotalChance();
        }

        public void StopWeaponBoost()
        {
            _allowedBoosts.Remove( _allowedBoosts.Find(o => o.boostType == BoostType.Weapon) );
            CalculateTotalChance();
        }
        

    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using Combat;
using UnityEngine;
using Random = UnityEngine.Random;

using Obstacles;
using UnityEngine.Assertions.Must;

namespace Managers
{


    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance;

        public EnemyManager enemyManager;
        public ObstacleManager obstacleManager;
        public BoostManager boostManager;
        public AudioManager audioManager;
        public GameObject playerInstance;

        [Header("Challenge toggles")]
        public bool causeTrouble;
        public bool enemyWaves;
        public bool enemyFormations;
        public bool obstacleRain;

        public GameDifficultiesSettings difficulty;
        [SerializeField] private int currentDifficulty;
        
        private void Awake()
        {
            if (Instance == null)
                Instance = this;

            EventManager.OnGameOverEvent += StopAllTroubles;
            EventManager.OnGameStartedEvent += InitializeGameplay;
        }

        private void Start()
        {
            StartGame();
        }

        public void StartGame()
        {
            InitializeGameplay();
            // StartCoroutine( IncreaseDifficultyOvertime() );
        }

        private void StopAllTroubles()
        {
            StopAllCoroutines();
        }

        private IEnumerator IncreaseDifficultyOvertime()
        {
            var untilNewDifficulty = new WaitForSeconds(10f);
            
            while (currentDifficulty < difficulty.gameDifficulties.Length-1)
            {
                currentDifficulty++;
                yield return untilNewDifficulty;
            }

            
        }

        private void InitializeGameplay()
        {
            InitializePlayer();

            StartChallengeGeneration();
        }

        private void StartChallengeGeneration()
        {
            StartCoroutine( GenerateChallenges() );
        }


        private IEnumerator GenerateChallenges()
        {
            while (causeTrouble)
            {
                GetChallenge();

                var thisDifficulty = difficulty.gameDifficulties[currentDifficulty];
                
                var waitForNextTrouble = new WaitForSeconds( Random.Range(thisDifficulty.minTimeUntilTrouble, thisDifficulty.maxTimeUntilTrouble) );
                yield return waitForNextTrouble;
            }
        }
        
        private void InitializePlayer()
        {
            // Spawn player, bring him into the game 
        }
        
        public void GetChallenge()
        {
            var randomChallengeID = Random.Range(0, 3);
            
            switch (randomChallengeID)
            {
                case 0: // Obstacle rain
                    if (obstacleRain)
                        obstacleManager.StartMeteorShower( Random.Range(2f, 5f) );
                    break;
                
                case 1: // Wave
                    if (enemyWaves)
                        enemyManager.StartWave();
                    break;
                
                case 2: // Formation
                    if (enemyFormations)
                        enemyManager.StartFormation();
                    break;
                
                
                case 3: // Boss
                    break;
            }
        }


        public List<int> GetAllowedEnemies()
        {
            var allowedEnemies = new List<int>() {0};

            if (difficulty.gameDifficulties[currentDifficulty ].useScorpions)
                allowedEnemies.Add(1);
            
            if (difficulty.gameDifficulties[currentDifficulty ].useVipers)
                allowedEnemies.Add(2);
            
            return allowedEnemies;
        }
    }
}

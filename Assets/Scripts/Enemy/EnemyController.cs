﻿using System;
using System.Collections;
using System.Collections.Generic;
using Managers;
using UnityEngine;
using Combat;
using Obstacles;
using Player;
using Unity.Mathematics;
using Utilities;

namespace Enemy
{
    public class EnemyController : MonoBehaviour
    {
        
        [Header("Spawning details")]
        public bool canShoot;
        [SerializeField] private HealthStat healthStat;
        [SerializeField] private EnemyType enemyType;
        [SerializeField] private int scorePoints;
        
        
        [Header("Combat parameters")]
        [SerializeField] private Transform[] projectileMuzzles;
        [SerializeField] private Projectile projectile;
        [SerializeField] private float attackSpeed;
        
        private GameObject _mortalEnemyGO;
        private bool _alive = true;

        private void Start()
        {
            if (!canShoot) return;
            
            FindYourEnemy();
            StartCoroutine( Shoot( ) );

        }
        
        public HealthStat Health()
        { 
            return healthStat;
        }

        private void FindYourEnemy()
        {
            _mortalEnemyGO = GameManager.Instance.playerInstance; // Gets the player instance
        }

        private IEnumerator Shoot()
        {
            var target = _mortalEnemyGO.transform.position;
            var shootingSpeed = new WaitForSeconds( 1 / attackSpeed );
            
            while (_alive)
            {
                foreach (var muzzle in projectileMuzzles)
                {
                    var newProjectile = Instantiate(projectile, muzzle.position, new Quaternion(0,0,180,0));
                    if (enemyType == EnemyType.Viper)
                    {
                        newProjectile.GetComponent<Rocket>().SendRocket(_mortalEnemyGO.transform);
                    } else if ( enemyType == EnemyType.Scorpion ) {
                        newProjectile.GetComponent<LaserBullet>().MoveBullet(true);
                    }

                }

                yield return shootingSpeed;
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            switch (other.tag)
            {
                case "Obstacle":
                    if (!healthStat.TakeDamage(other.gameObject.GetComponent<EnemyController>().healthStat.health))
                        PlayDead();
                    break;
                case "Player":
                    if (!healthStat.TakeDamage(other.gameObject.GetComponent<PlayerController>().Health().health))
                        PlayDead();
                    break;
                case "PlayerProjectile":

                    var hitProjectile = other.gameObject.GetComponent<Projectile>();
                    if (!healthStat.TakeDamage(hitProjectile.damage))
                        PlayDead();

                    hitProjectile.DestroySelf();
                    break;
            }
            
            // var collisionType = CollisionChecker.ShouldTakeDamage(gameObject.tag, other.tag);
            //
            // switch (collisionType)
            // {
            //     case CollisionType.Damage:
            //     {
            //         var damageToTake = 0;
            //         
            //         if (other.CompareTag("Obstacle"))
            //         {
            //             damageToTake = other.gameObject.GetComponent<ObstacleController>().obstacleDamage;
            //         } else if (other.CompareTag("PlayerProjectile"))
            //         {
            //             damageToTake = other.gameObject.GetComponent<Projectile>().damage;
            //         }
            //
            //         if (healthStat.TakeDamage(damageToTake))
            //         {
            //             Debug.Log("Got out of hp " + healthStat.health + " collided with " + other.name);
            //             PlayDead();
            //         }
            //
            //     
            //         Destroy(other.gameObject); // Destroy the collider entering
            //         break;
            //     }
            //     case CollisionType.Destruction:
            //         Debug.Log("Got destruction collision");
            //         PlayDead();
            //         break;
            // }
        }

        private void PlayDead()
        {
            if (!_alive)
                return;
            
            _alive = false;
            GameManager.Instance.boostManager.TrySpawningBoost(transform);
            GameManager.Instance.audioManager.PlaySound(SoundEffect.Explosion);
            EventManager.OnScoreAdded(scorePoints);
            // Play explosion effect, etc.
            Destroy(gameObject); // Destroy the enemy // TODO: call gameover event from here
        }
        
        
        // TODO: THIS HAS TO BE CHANGED!!! Always gives errors about enemies getting destroyed, putting Yield break did not help.
        public IEnumerator MoveToDestination(Transform destination, float duration)
        {
            var startingPos = transform.position;
            
            var startTime = 0f;
        
            while ((startTime + Time.deltaTime) < duration)
            {
                if (!_alive)
                    yield break;
                
                startTime += Time.deltaTime;
                var distanceRatio = startTime / duration;
                transform.position = Vector3.Lerp(startingPos, destination.position,  distanceRatio);

                yield return null;
            }
        }
    }
}
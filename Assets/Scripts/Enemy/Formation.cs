﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

//Custom namespaces
using Enemy;
using Paths;
using Managers;

namespace Combat
{
    public class Formation : MonoBehaviour
    {
        public EnemyController enemyToSpawn;
        public Transform[] formationPositions;
        public Transform[] formationPath;
        public float formationSpeed;

        
        private List<EnemyController> _spawnedEnemies = new List<EnemyController>();
        
        public IEnumerator InitializeFormation(Transform[] spawnPositions)
        {

            // var enemyPositions = new List<Vector3>();

            for (var i = 0; i < formationPositions.Length; i++)
            {
                var formationPoint = formationPositions[i];
                var spawnedEnemy = Instantiate(enemyToSpawn, transform);
                spawnedEnemy.transform.position = spawnPositions[Random.Range(0, spawnPositions.Length)].position;
                _spawnedEnemies.Add(spawnedEnemy);
                // enemyPositions.Add(spawnedEnemy.transform.position); // Add to enemy position dictionary

                StartCoroutine( spawnedEnemy.MoveToDestination(formationPositions[i], formationSpeed));
            }

            yield return new WaitForSeconds(formationSpeed);
            
            // Send formation to follow path
            gameObject.AddComponent<PathFollow>().SetPathsAndSpeed(formationPath, 0.15f);

            StartCoroutine(CheckIfStillAlive());
        }
        
        private IEnumerator CheckIfStillAlive()
        {
            var waitToCheck = new WaitForSeconds( 0.1f );
            
            Debug.Log("Starting to check for live enemies");

            
            while (CheckForSurvivors(_spawnedEnemies))
            {
                yield return waitToCheck;
            }

            EventManager.OnFormationCleared();
            Destroy(gameObject);
        }

        private static bool CheckForSurvivors(List<EnemyController> totalEnemies)
        {
            return totalEnemies.Any(enemy => enemy != null);
        }
    }
}

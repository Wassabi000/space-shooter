﻿using System.Collections;
using System.Collections.Generic;
using Paths;
using UnityEngine;

namespace Enemy
{
    public class Wave : MonoBehaviour
    {
        [Header("Enemy details")]
        public EnemyController enemyToSpawn;
        public int enemyCount;
        public float speed;
        
        [Header("Pattern")]
        public Transform[] wavePath;
        public float spawnDelay;
        
        [Header("Mirror Wave")]
        public bool isMirrored;
        public Transform[] wavePathMirror;
        public float mirrorredWaveDelay;
        
        private List<EnemyController> _spawnedEnemies = new List<EnemyController>();

        public void InitializeWave(Transform spawnPosition)
        {
            StartCoroutine( SpawnWaves(spawnPosition) );
        }

        // Spawns wave, two if mirrored
        private IEnumerator SpawnWaves(Transform spawnPosition)
        {
            var wavesCount = isMirrored ? 2 : 1;
            
            for (var i = 0; i < wavesCount; i++)
            {
                var path= i > 0 ? wavePathMirror : wavePath;
                
                StartCoroutine( SpawnWave(spawnPosition, path) );
                yield return new WaitForSeconds(mirrorredWaveDelay);
            }
        }

        private IEnumerator SpawnWave(Transform spawnPosition, Transform[] pathToFollow) // TODO: Implement wave mirroring
        {
            var enemyCountdown = enemyCount; 
            while (enemyCountdown > 0)
            {
                var spawnedEnemy = Instantiate(enemyToSpawn, transform);
                spawnedEnemy.transform.position = spawnPosition.position;
                
                var pathFollow = spawnedEnemy.gameObject.AddComponent<PathFollow>();
                pathFollow.SetPathsAndSpeed(pathToFollow, speed);
                
                _spawnedEnemies.Add(spawnedEnemy);
                enemyCountdown--;
                
                var waitUntilNextShip = new WaitForSeconds( spawnDelay );
                yield return waitUntilNextShip;
            }
            
            StartCoroutine(CheckIfStillAlive());
        }

        private IEnumerator CheckIfStillAlive()
        {
            while (_spawnedEnemies.Count > 0)
            {
                var waitToCheck = new WaitForSeconds( 0.2f );
                yield return waitToCheck;
            }

            Destroy(gameObject);
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Paths
{
    public class PathFollow : MonoBehaviour
    {
        public Transform[] paths;
        
        private float _speed;
        private int _pathToFollow;
        private float _timePassed;
        private Vector2 _objectPosition;
        private bool _coroutineAllowed;

        private void Start()
        {
            _pathToFollow = 0;
            _timePassed = 0f;
            _coroutineAllowed = true;
        }

        private void Update()
        {
            if (_coroutineAllowed)
                StartCoroutine(GoByTheRoute(_pathToFollow));
        }

        public void SetPathsAndSpeed(Transform[] pathsToFollow, float givenSpeed)
        {
            paths = pathsToFollow;
            _speed = givenSpeed;
        }

        public void BeginPaths()
        {
            if (_coroutineAllowed)
                StartCoroutine(GoByTheRoute(_pathToFollow));
        }

        private IEnumerator GoByTheRoute(int routeNumber)
        {
            _coroutineAllowed = false;

            Vector2 p0 = paths[routeNumber].GetChild(0).position;
            Vector2 p1 = paths[routeNumber].GetChild(1).position;
            Vector2 p2 = paths[routeNumber].GetChild(2).position;
            Vector2 p3 = paths[routeNumber].GetChild(3).position;

            while (_timePassed < 1)
            {
                _timePassed += Time.deltaTime * _speed;

                _objectPosition = Mathf.Pow(1 - _timePassed, 3) * p0 +
                              3 * Mathf.Pow(1 - _timePassed, 2) * _timePassed * p1 +
                              3 * (1 - _timePassed) * Mathf.Pow(_timePassed, 2) * p2 +
                              Mathf.Pow(_timePassed, 3) * p3;

                transform.position = _objectPosition;
                yield return new WaitForEndOfFrame();
            }

            _timePassed = 0f;

            _pathToFollow++;

            if (_pathToFollow > paths.Length - 1)
                _pathToFollow = 0;

            _coroutineAllowed = true;
        }
    }
}
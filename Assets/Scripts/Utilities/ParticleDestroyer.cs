﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    [RequireComponent(typeof(ParticleSystem))]
    public class ParticleDestroyer : MonoBehaviour
    {
        // To keep re-active particles instead of re-instantiating
        public bool _onlyDeactivate;

        private void OnEnable()
        {
            StartCoroutine(CheckIfAlive());
        }

        private IEnumerator CheckIfAlive()
        {
            var particles = this.GetComponent<ParticleSystem>();

            while (true && particles != null)
            {
                yield return new WaitForSeconds(0.5f);
                if (particles.IsAlive(true)) continue;
                
                if (_onlyDeactivate)
                {
                    this.gameObject.SetActive(false);
                } else {
                    GameObject.Destroy(this.gameObject);
                }
            }
        }
    }
}
﻿using System;
using Managers;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class FuelBar : MonoBehaviour
    {
        
        [SerializeField] private Image _fuelFill;
        
        private void Start()
        {
            EventManager.OnFuelDecreasedEvent += UpdateFuelBar;
        }

        private void UpdateFuelBar(float currentFuel)
        {
            _fuelFill.fillAmount = currentFuel;
        }
    }
}
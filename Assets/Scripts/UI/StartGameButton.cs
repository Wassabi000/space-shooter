﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class StartGameButton : MonoBehaviour
    {
        public void StartGame()
        {
            SceneManager.LoadScene("GameplayScene");
        }
    }
}

﻿using Managers;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class HealthBar : MonoBehaviour
    {
        [SerializeField] private Image _healthFIll;
        
        private void Start()
        {
            EventManager.OnPlayerHealthChangedEvent += UpdateHealthBar;
        }

        private void UpdateHealthBar(float currentHp)
        {
            _healthFIll.fillAmount = currentHp;
        }
    }
}
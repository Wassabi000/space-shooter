﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class HomeButton : MonoBehaviour
    {
        public void GoHome()
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
}
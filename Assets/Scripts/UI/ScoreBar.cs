﻿using System;
using Managers;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class ScoreBar : MonoBehaviour
    {
        
        public Text scoreText;
        
        private void OnEnable()
        {
            EventManager.OnScoreAddedEvent += UpdateScore;
        }
        
        private void OnDisable()
        {
            EventManager.OnScoreAddedEvent -= UpdateScore;
        }

        private void UpdateScore(int pointsToAdd)
        {
            // int currentScore;
            // if (!int.TryParse(scoreText.text, out currentScore))
            //     currentScore = 0;
            
            scoreText.text = (int.Parse(scoreText.text) + pointsToAdd).ToString();
        }

        
    }
}
﻿using UnityEngine;

namespace Combat
{
    [CreateAssetMenu(fileName = "Game Difficulty", menuName = "Game Difficulties", order = 0)]
    public class GameDifficultiesSettings : ScriptableObject
    {
        public Difficulty[] gameDifficulties;
    }
}
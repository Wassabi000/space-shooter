﻿namespace Combat
{
    [System.Serializable]
    public class Difficulty
    {
        public int id;
        public float minTimeUntilTrouble;
        public float maxTimeUntilTrouble;
        public bool useScorpions;
        public bool useVipers;
    }
}
﻿using Combat;
using UnityEngine;

namespace Player
{
    [System.Serializable]
    public class PlayerWeapon
    {
        public string name;
        public float shootingSpeed;
        public Transform[] gunPoint;
        public ParticleSystem[] muzzleEffect;
        public Projectile projectile;
        public GameObject weaponGO;

        public PlayerWeapon()
        {
            
        }
    }

}
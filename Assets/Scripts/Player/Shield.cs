﻿using Combat;
using Enemy;
using Obstacles;
using UnityEngine;
using Utilities;

namespace Player
{
    public class Shield : MonoBehaviour
    {
        public bool shieldIsActive;
        [SerializeField] private HealthStat shieldHealth;
        
        
        public HealthStat Health()
        { 
            return shieldHealth;
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            
            switch (other.tag)
            {
                case "Enemy":
                    if (!shieldHealth.TakeDamage(other.gameObject.GetComponent<EnemyController>().Health().health))
                        DestroySelf();
                    break;
                
                case "Obstacle":
                    if (!shieldHealth.TakeDamage(other.gameObject.GetComponent<ObstacleController>().healthStat.health))
                        DestroySelf();
                    break;
                
                case "Projectile":
                    if (!shieldHealth.TakeDamage(other.gameObject.GetComponent<Projectile>().damage))
                        DestroySelf();
                    break;
            }
            
            // var collisionType = CollisionChecker.ShouldTakeDamage(gameObject.tag, other.tag);
            //
            // switch (collisionType)
            // {
            //     case CollisionType.Damage:
            //     {
            //         if(other.CompareTag("PlayerProjectile") || other.CompareTag("Player")) // TODO: Should be moved out to the main checker script
            //             return;
            //             
            //         Debug.Log("Boost: Got damage");
            //         if (other.CompareTag("Obstacle"))
            //         {
            //             var obstacle = other.GetComponent<ObstacleController>();
            //             if (obstacle.healthStat.health > shieldHealth.health)
            //             {
            //                 DestroySelf();
            //             } else {
            //                 obstacle.PlayDead();
            //             }
            //         }
            //
            //         if (shieldHealth.TakeDamage(other.gameObject.GetComponent<Projectile>().damage))
            //             DestroySelf();
            //     
            //         Destroy(other.gameObject); // Destroy the collider entering
            //         break;
            //     }
            //     case CollisionType.Destruction:
            //         Debug.Log("Boost: Got destruction");
            //         DestroySelf();
            //         break;
            // }
        }
        
        private void DestroySelf()
        {
            GetComponent<ParticleSystem>().Stop();
            Destroy(gameObject, 0.5f);
        }
    }
}
﻿using Combat;
using UnityEngine;

namespace Player
{
    [System.Serializable]
    public class PlayerArmor
    {
        public string name;
        public GameObject armorGO;
        public int extraHealth;
        public int extraFuel;

        public PlayerArmor()
        {
            
        }
    }

}
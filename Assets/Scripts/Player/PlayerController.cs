﻿using System.Collections;
using System.Collections.Generic;
using Combat;
using Enemy;
using Managers;
using Obstacles;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Player
{
    public class PlayerController : MonoBehaviour, IDragHandler
    {
        public bool shoot;

        [Header("Combat")] public List<PlayerWeapon> playerWeapons = new List<PlayerWeapon>();
        public List<PlayerArmor> playerArmors = new List<PlayerArmor>();

        [SerializeField] private HealthStat healthStat;
        [SerializeField] private FuelStat fuelStat;
        [SerializeField] private GameObject explosionParticle;

        private int _armorUpgradesUsed;
        private int _weaponUpgradesUsed = 1;

        public HealthStat Health()
        { 
            return healthStat;
        }

        #region Initialization
        
        // Start is called before the first frame update
        private void Start()
        {
            GameManager.Instance.playerInstance = gameObject; // Tell the game who the player is

            DisableAllUpgrades();
        
            if (shoot)
                for (var i = 0; i < _weaponUpgradesUsed; i++)
                    StartCoroutine(ShootForever(playerWeapons[i]) ); // Starts your never ending shooting barrage

            StartCoroutine(KeepLosingFuel() ); //Start helplessly losing fuel
        }

        private void DisableAllUpgrades()
        {
            foreach (var armor in playerArmors)
            {
                armor.armorGO.SetActive(false);
            }

            for (var i = 1; i < playerWeapons.Count; i++)
            {
                var weapon = playerWeapons[i];
                weapon.weaponGO.SetActive(false);
            }
        }
        
        #endregion
        public void OnDrag(PointerEventData eventData)
        {
            var pointerPos = Camera.main.ScreenToWorldPoint(eventData.position);
            transform.position = new Vector3(pointerPos.x, pointerPos.y, 0);
        }

        #region Collision/Death

        private void OnTriggerEnter2D(Collider2D other)
        {
            var playerTookDamage = false;
         
            switch (other.tag)
            {
                case "Enemy":
                    if (!healthStat.TakeDamage(other.gameObject.GetComponent<EnemyController>().Health().maxHealth))
                        PlayDead();

                    playerTookDamage = true;
                    break;
                
                case "Obstacle":
                    if (!healthStat.TakeDamage(other.gameObject.GetComponent<ObstacleController>().healthStat.maxHealth))
                        PlayDead();
                    
                    playerTookDamage = true;
                    break;
                
                case "Boost":
                    GetBoost(other.GetComponent<Boost>());
                    break;
                
                case "Projectile":
                    if (!healthStat.TakeDamage(other.gameObject.GetComponent<Projectile>().damage))
                        PlayDead();
                    
                    playerTookDamage = true;
                    break;
            }

            if (playerTookDamage)
                EventManager.Instance.OnPlayerHealthChanged((float)healthStat.health / healthStat.maxHealth );
        }

        private void PlayDead()
        {
            // Play explosion effect, etc.
            Instantiate(explosionParticle, transform.position, quaternion.identity);
            GameManager.Instance.audioManager.PlaySound(SoundEffect.Explosion);

            EventManager.Instance.OnGameOver();
            
            Destroy(gameObject); // Destroy the player 
        }
        
        #endregion

        #region Boost/Upgrades

        
        private void GetBoost(Boost boost)
        {
            switch (boost.boostType)
            {
                case BoostType.Shield:
                    boost.GetComponent<ShieldBoost>().EnableShield(transform);
                    break;
                case BoostType.Fuel:
                    fuelStat.GetFuel( boost.GetComponent<FuelBoost>().fuelToAdd );
                    Destroy(boost.gameObject);
                    break;
                
                case BoostType.Repair:
                    healthStat.GetRepaired();
                    EventManager.Instance.OnPlayerHealthChanged((float)healthStat.health / healthStat.maxHealth );
                    Destroy(boost.gameObject);
                    break;
                
                case BoostType.Armor:
                    TryUpgrading(true);
                    Destroy(boost.gameObject);
                    break;
                
                case BoostType.Weapon:
                    TryUpgrading(false);
                    Destroy(boost.gameObject);
                    break;
            }
        }

        private void TryUpgrading(bool armorUpgrade)
        {
            if (armorUpgrade)
            {
                if (_armorUpgradesUsed < playerArmors.Count)
                {
                    var armorToAdd = playerArmors[_armorUpgradesUsed];
                    armorToAdd.armorGO.SetActive(true);
                    healthStat.GetMoreMaxHP(armorToAdd.extraHealth);
                    fuelStat.GetMoreMaxFuel(armorToAdd.extraHealth);
                    _armorUpgradesUsed++;
                } else {
                    GameManager.Instance.boostManager.StopArmorBoost();
                }

            } else {

                if (_weaponUpgradesUsed < playerWeapons.Count)
                {
                    var weaponToAdd = playerWeapons[_weaponUpgradesUsed];
                    weaponToAdd.weaponGO.SetActive(true);
                    StartCoroutine(ShootForever(playerWeapons[_weaponUpgradesUsed]));
                    _weaponUpgradesUsed++;
                } else {
                    GameManager.Instance.boostManager.StopWeaponBoost();
                }

            }
        }

        #endregion

        #region Shooting/Fuel Coroutines

        private static IEnumerator ShootForever(PlayerWeapon weaponToShoot)
        {
            var waitForNextShot = new WaitForSeconds( 1 / weaponToShoot.shootingSpeed );

            while (true)
            {
                for (var i = 0; i < weaponToShoot.gunPoint.Length; i++)
                {
                    var muzzle = weaponToShoot.gunPoint[i];
                    var bullet = Instantiate(weaponToShoot.projectile, muzzle.position, Quaternion.identity);
                    bullet.GetComponent<LaserBullet>().MoveBullet(); // Moves up by default
                    weaponToShoot.muzzleEffect[i].Play();
                }


                yield return waitForNextShot;
            }
        }

        private IEnumerator KeepLosingFuel()
        {
            var stillGotFuel = true;
            
            while (stillGotFuel)
            {
                stillGotFuel = fuelStat.LoseFuel(fuelStat.consumptionPerSecond);
                
                if (!stillGotFuel)
                    yield return null;
                
                EventManager.OnFuelDecreased( fuelStat.fuel / fuelStat.maximumFuel );
                
                var waitFOrFuelLoss = new WaitForSeconds(1f);
                yield return waitFOrFuelLoss;
            }
        }
        
        #endregion
    }
}

﻿namespace Player
{
    [System.Serializable]
    public class FuelStat
    {
        public float fuel;
        public float maximumFuel;
        public float consumptionPerSecond;

        public FuelStat(FuelStat stat)
        {
            fuel = stat.fuel;
        }

        // Check if you can lose fuel. If not return false, If yes, lose it.
        public bool LoseFuel(float fuelToLose)
        {
            if (!(fuel > fuelToLose)) return false;
            fuel -= fuelToLose;
            return true;
        }

        public void GetFuel(float fuelToAdd)
        {
            if ((fuel + fuelToAdd) > maximumFuel)
            {
                fuel = maximumFuel;
                return;
            }

            fuel += fuelToAdd;
        }

        public void GetMoreMaxFuel(float fuelIncrease)
        {
            maximumFuel += fuelIncrease;
            fuel += fuelIncrease;
        }

    }
}
﻿using System.Collections;
using UnityEngine;
using Utilities;

namespace Combat
{
    [System.Serializable]
    public class ShieldBoost : Boost
    {
        public GameObject effectPrefab;

        public void EnableShield(Transform playerPos)
        {
            gameObject.GetComponentInChildren<SpriteRenderer>().enabled = false;
            Instantiate(effectPrefab, playerPos); // Start the particle effect;
        }
    }
}
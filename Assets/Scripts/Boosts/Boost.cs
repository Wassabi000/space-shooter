﻿using System;
using Managers;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Combat
{
    
    [System.Serializable]
    public class Boost : MonoBehaviour
    {
        public BoostType boostType;

        [Range(1, 10)] public int spawnChance;
            
        [SerializeField] private float velocity;
        [SerializeField] private float gravity;
        
        private void Start()
        {
            // Do Boost movement 
            // Go down and wiggle left/right
            var rigid2D = gameObject.GetComponent<Rigidbody2D>();
            rigid2D.gravityScale = gravity; // Make the boost keep falling
            rigid2D.velocity = new Vector2( Random.Range(-1, 2), 0) * velocity; // Randomly push the boost to the side
        }
    }
}
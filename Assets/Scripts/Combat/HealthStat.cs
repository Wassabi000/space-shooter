﻿namespace Combat
{
    [System.Serializable]
    public class HealthStat
    {
        public int health;
        public int maxHealth;

        public HealthStat(HealthStat stat)
        {
            health = stat.health;
            maxHealth = stat.maxHealth;
        }

        public bool TakeDamage(int damageToTake)
        {
            if ((health - damageToTake) < 0)
                return false;

            health -= damageToTake;
            return true;
        }

        public void GetRepaired()
        {
            health = maxHealth;
        }

        public void GetMoreMaxHP(int hpToAdd)
        {
            maxHealth += hpToAdd;
            health += hpToAdd;
        }
    }
}
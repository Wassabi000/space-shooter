﻿using System;
using System.Collections;
using Managers;
using UnityEngine;

namespace Combat
{
    public class Rocket : Projectile
    {
        [Header("Rocket specific")]
        [SerializeField] private float rotateSpeed;

        public void SendRocket(Transform playerPos)
        {
            StartCoroutine(ChasePlayer(playerPos));
        }

        private IEnumerator ChasePlayer(Transform playerPos)
        {
            var updateTime = new WaitForSeconds(0.1f);
            var rocketBody = GetComponent<Rigidbody2D>();
            
            while (true)
            {
                var direction = (Vector2)playerPos.position - rocketBody.position;
                direction.Normalize ();
                
                var rotateAmount = Vector3.Cross (direction, transform.up).z;
                rocketBody.angularVelocity = -rotateSpeed * rotateAmount;
                rocketBody.velocity = transform.up * projectileSpeed;
                
                yield return updateTime;
            }
        }
    }
}
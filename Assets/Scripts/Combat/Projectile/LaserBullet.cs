﻿using UnityEngine;

namespace Combat
{
    public class LaserBullet : Projectile
    {

        // Projectile is blissfully sent on it's way
        public void MoveBullet(bool moveUp = true)
        {
            if (moveUp)
            {
                gameObject.GetComponent<Rigidbody2D>().velocity = transform.up * projectileSpeed;
            } else {
                gameObject.GetComponent<Rigidbody2D>().velocity = -transform.up * projectileSpeed;
            }
        }
    }
}
﻿using System;
using Managers;
using Obstacles;
using UnityEngine;
using Utilities;

namespace Combat
{
    public abstract class Projectile : MonoBehaviour
    {
        public int damage;
        public float projectileSpeed;
        
        [Header("Sounds")]
        [SerializeField] private SoundEffect hitSound;
        
        [Header("Particles")]
        [SerializeField] private ParticleSystem projectileHit;


        // Do particles and cool stuff
        public void DestroySelf()
        {
            GameManager.Instance.audioManager.PlaySound(hitSound);
            var onHit = Instantiate(projectileHit, transform.position, Quaternion.identity);
            onHit.Play();
            
            Destroy(gameObject);
        }

    }
}
﻿using System;
using UnityEngine;

namespace DefaultNamespace
{
    public class BackgroundScroll : MonoBehaviour
    {
        public Renderer backgroundRend;
        public float scrollSpeed;
        
        private void Update()
        {
            backgroundRend.material.mainTextureOffset += new Vector2(0f, scrollSpeed * Time.deltaTime);
        }
    }
}
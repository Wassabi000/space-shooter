﻿using Combat;
using Managers;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;
using Utilities;

namespace Obstacles
{
    [System.Serializable]
    public class ObstacleController : MonoBehaviour
    {
        public int obstacleDamage;
        public HealthStat healthStat;
        
        [HideInInspector] public float passiveRotation;
        [HideInInspector] public Vector2 destinationPoint;
        [HideInInspector] public float speed;

        [SerializeField] private bool _isParent;
        [SerializeField] private ObstacleController[] childObstacles;
        [SerializeField] private float shardSpeed;
        [SerializeField] private GameObject explosionParticles;
        
        private int scorePoints = 10; // TODO: change this to a variable in a scriptable object later on.

        private void Start()
        {
            var rigid2D = gameObject.GetComponent<Rigidbody2D>();
            
            rigid2D.velocity = destinationPoint * speed;
            rigid2D.angularVelocity = Random.Range(-passiveRotation, passiveRotation);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            switch (other.tag)
            {
                case "Enemy":
                case "Obstacle":
                case "Player":
                    PlayDead();
                    break;
                
                case "Projectile":
                case "PlayerProjectile":
                    GameManager.Instance.audioManager.PlaySound(SoundEffect.RockHit);
                    other.GetComponent<Projectile>().DestroySelf();
                    PlayDead();
                    break;
            }
        }

        public void PlayDead()
        {
            GetComponent<CircleCollider2D>().enabled = false;
            SpawnParticles(explosionParticles, transform);
            GameManager.Instance.audioManager.PlaySound(SoundEffect.RockBreak);
            
            if (_isParent)
            {
                var newVelocity = gameObject.GetComponent<Rigidbody2D>().velocity + Random.insideUnitCircle;

                foreach (var shard in childObstacles)
                {
                    shard.transform.SetParent( transform.parent ); // Set spawn position as shard's parent to avoid getting destroyed with the core
                    shard.GetComponent<CircleCollider2D>().enabled = true;
                    var rigid2D = shard.GetComponent<Rigidbody2D>();

                    rigid2D.bodyType = RigidbodyType2D.Dynamic;
                    rigid2D.velocity = newVelocity;
                    rigid2D.angularVelocity = Random.Range(-passiveRotation, passiveRotation);
                }
            }
            
            EventManager.OnScoreAdded(scorePoints);
            // Do additional stuff
            Destroy(gameObject); // Destroy the obstacle
        }

        private static void SpawnParticles(GameObject goToSpawn, Transform ship)
        {
            Instantiate(goToSpawn, ship.position, quaternion.identity);
        }
    }
}

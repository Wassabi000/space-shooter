﻿
using System;
using UnityEngine;

namespace Obstacles
{
    public class TheEdgeOfDestruction : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            // Debug.Log("Entered: " + other.tag);
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            // Debug.Log("Exited: " + other.tag);
            Destroy(other.gameObject);
        }
    }
}
